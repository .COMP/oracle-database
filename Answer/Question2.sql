/*--------------------2.1--------------------*/
CREATE OR REPLACE TRIGGER PreventDeletePassenger
BEFORE DELETE
ON Passenger
BEGIN
RAISE_APPLICATION_ERROR(-20111,'Can not delete any passenger...');
END;

/*For testing*/
delete from passenger where passenger_id = 1;

/*Test insert*/
insert into Passenger 
(first_name, last_name, phone_no, email, address, zip_code, state, city, country)
values
('ali', 'abd alrahman', '0966973196', 'yamen@gmail.com',
'al tall', '5361', 'my state', 'syria', 'damascus');
commit;

/*Test delete*/
delete from Passenger;

/*--------------------2.2--------------------*/
CREATE OR REPLACE TRIGGER PreventUpdateBookingDate
BEFORE UPDATE
OF booking_date
ON Booking
REFERENCING OLD AS old NEW AS new
FOR EACH ROW
DECLARE 
    booking_status VARCHAR(50);
BEGIN
     SELECT booking_status_code into booking_status
     FROM BookingStatus
     WHERE BookingStatus.booking_status_id = :old.booking_status_id;

     if booking_status = 'not refundable'
     THEN
         RAISE_APPLICATION_ERROR(-20111,'Can not update booking date because booking status is (not refundable)...');
    END IF;
END;

/*Test update date where booking status is not refundable*/
update Booking set booking_date = TO_DATE('2044-02-04', 'yy-mm-dd') WHERE booking_status_id = 2;
select * from booking;

/*--------------------2.3--------------------*/
CREATE OR REPLACE TRIGGER PreventAddUnValidBooking
BEFORE INSERT
ON Booking
REFERENCING OLD AS old NEW AS new
FOR EACH ROW
DECLARE 
    booking_count NUMBER;
    flight_id_before NUMBER;
    before_arival_time TIMESTAMP;
    after_departure_time TIMESTAMP;
BEGIN
    select count(*) INTO  booking_count from booking;
    IF(booking_count>0)
    THEN
        /*Fetch last-first flight id of current passenger*/
        SELECT flight_id INTO  flight_id_before FROM Booking
        WHERE  passenger_id = :new.passenger_id and ROWNUM = 1
        ORDER BY booking_id ASC;
        /*Fetch arival time of last booking*/
        SELECT flight2.arival_time INTO before_arival_time  FROM Flight flight2 WHERE flight2.flight_id =  flight_id_before;
        /*Fetch departure time of new booking*/
        SELECT flight.departure_time INTO after_departure_time FROM Flight flight WHERE flight.flight_id = :new.flight_id;
        IF(before_arival_time > after_departure_time )
        THEN
            RAISE_APPLICATION_ERROR(-20111,'Can not insert bookingin this date');
            ROLLBACK;
        END IF;
    END IF; 
END;


drop trigger PreventAddUnValidBooking;
select * from Flight;
delete from Booking;

/*--------------------2.4--------------------*/
/*Create audit booking*/
CREATE TABLE AuditBooking(
audit_seq NUMBER(10),   
audit_kind VARCHAR2(10),
audit_date DATE,
old_booking_id NUMBER,
old_booking_date DATE,
old_passenger_id NUMBER,
old_booking_status_id NUMBER,
old_ticket_type_id NUMBER,
old_flight_id NUMBER,
old_travle_class_id NUMBER
);
/
/*Create sequence for audit booking*/
CREATE SEQUENCE AuditBookingSequence
MINVALUE 1
NOMAXVALUE
INCREMENT BY 1
START WITH 1
NOCACHE
ORDER
NOCYCLE
;
/

/*Create trigger for sequence audit booking*/
CREATE OR REPLACE TRIGGER AuditBookingTriggerForSequence
BEFORE INSERT
ON AuditBooking
FOR EACH ROW
BEGIN
 SELECT AuditBookingSequence.NEXTVAL
 INTO :NEW.audit_seq
 FROM DUAL;
EXCEPTION
 WHEN OTHERS THEN
RAISE_APPLICATION_ERROR(-20111, 'Can not generate seq value');
END;
/

/*Create trigger for audit*/
CREATE OR REPLACE TRIGGER AuditBookingTrigger
BEFORE INSERT OR UPDATE OR DELETE
ON Booking
REFERENCING OLD as old NEW AS new
FOR EACH ROW
DECLARE
 v_audit_kind VARCHAR2(10);
BEGIN
 IF INSERTING THEN
 v_audit_kind := 'Insert';
 ELSIF UPDATING THEN
 v_audit_kind := 'Update';
 ELSIF DELETING THEN
 v_audit_kind := 'Delete';
 END IF;
 
 INSERT INTO AuditBooking 
 ( audit_kind, audit_date, old_booking_id, old_booking_date, old_passenger_id,
    old_booking_status_id, old_ticket_type_id, old_flight_id, old_travle_class_id)
 VALUES
 (  v_audit_kind, SYSDATE, :old.booking_id, :old.booking_date, :old.passenger_id,
    :old.booking_status_id,:old.ticket_type_id, :old.flight_id , :old.travle_class_id);
END;

/*For testing*/
INSERT INTO Booking
(booking_date, passenger_id, booking_status_id, ticket_type_id, flight_id, travle_class_id)
VALUES
(TO_DATE('2055-02-02', 'yyyy-mm-dd'), 1, 3, 1, 1, 1)
;

delete from AuditBooking where booking_id=37;
select * from AuditBooking;
select * from Passenger;
select * from BookingStatus;
select * from Booking;
select * from TicketType;
select * from Flight;
select * from TravelClass;
select * from AuditBooking;

/*--------------------2.5--------------------*/
/*Create DDLIndexTable*/
CREATE TABLE DDLIndexTable (
index_name 		VARCHAR2(30),
operation_name 	VARCHAR2(30),
operation_date 	TIMESTAMP
);

/*Create trigger for create and delete index*/
CREATE OR REPLACE TRIGGER 
  DDLIndexTrigger
  AFTER CREATE OR DROP ON SCHEMA
BEGIN
  IF SYS.DICTIONARY_OBJ_TYPE = 'INDEX' THEN
      INSERT INTO DDLIndexTable
      SELECT ora_dict_obj_name, ora_sysevent, SYSDATE
      FROM DUAL;
  END IF;
END;


/*For testing*/
CREATE TABLE test(test_id NUMBER);

CREATE INDEX myIndex ON test(test_id);

DROP index myIndex;

SELECT * FROM DDLIndexTable;






