/*3.1 - 3.2*/
/* Create time table*/
CREATE TABLE TimeTable(
date_id date,
days NUMBER NOT NULL,
week number not null,
months varchar(10) NOT NULL,
quarter varchar(20) NOT NULL,
years NUMBER NOT NULL,
CONSTRAINT date_id_pk PRIMARY KEY(date_id)
)

/* Create time dimention*/
CREATE DIMENSION TimeDimension 
LEVEL date_dim_id IS(TimeTable.date_id)
LEVEL day  IS (TimeTable.days)
LEVEL week  IS (TimeTable.week)
LEVEL month  IS (TimeTable.months)
LEVEL Quarter  IS (TimeTable.quarter)
LEVEL Year  IS (TimeTable.years)
HIERARCHY first_hierarchy(
    date_dim_id CHILD OF 
    day CHILD OF
    month CHILD OF
    quarter CHILD OF
    year
)
    
HIERARCHY second_hierarchy(
    date_dim_id CHILD OF 
    week CHILD OF 
    month CHILD OF 
    year)
;
/*Create passenger dimeantion*/
CREATE DIMENSION PassengerDim 
LEVEL passenger_dim_id IS (Passenger.passenger_id)
LEVEL address  IS (Passenger.address)
LEVEL city  IS (Passenger.city)
LEVEL state  IS (Passenger.state)
LEVEL country IS (Passenger.country)

HIERARCHY PassengerHierarchy (
 passenger_dim_id child of 
 address child of 
 city child of 
 state child of 
 country
)

ATTRIBUTE passenger_dim_id DETERMINES 
(   Passenger.first_name,
    Passenger.last_name,
    Passenger.email,
    Passenger.phone_no,
    Passenger.zip_code
)
;


/*Create flight dimeantion*/
CREATE DIMENSION FlightDim 
LEVEL flight_dim_id IS (Flight.flight_id)
LEVEL air_plane_dim_id IS (AirPlane.air_plane_id)

ATTRIBUTE flight_dim_id DETERMINES 
(   Flight.flight_number,
    Flight.destination,
    Flight.departure_time,
    Flight.arival_time
)
ATTRIBUTE air_plane_dim_id DETERMINES
(
    Airplane.air_craft_type
)
;

/*Create travel dimention*/
CREATE DIMENSION TravelDim 
LEVEL travel_class_dim_id IS (travelclass.travel_class_id)
ATTRIBUTE travel_class_dim_id DETERMINES (travelclass.travel_class_core)
;

/*Create ticket type*/
CREATE DIMENSION TicketTypeDim 
LEVEL ticket_type_dim_id IS (tickettype.ticket_type_id)
ATTRIBUTE ticket_type_dim_id DETERMINES (tickettype.ticket_type_code)
;

/*Create booking status dimention*/
CREATE DIMENSION BookingStatusDim  
LEVEL booking_status_dim_id IS (bookingstatus.booking_status_id)
ATTRIBUTE booking_status_dim_id DETERMINES (bookingstatus.booking_status_code)
;

/*Create payment method dimention*/
CREATE DIMENSION PaymentMethodDim 
LEVEL payment_method_dim_id IS (paymentmethod.payment_method_id)
ATTRIBUTE payment_method_dim_id DETERMINES (paymentmethod.payment_method_code)
;

/*Create payment status dimention*/
CREATE DIMENSION PaymentStatusDim
LEVEL payment_status_dim_id IS (paymentstatus.payment_status_id)
ATTRIBUTE payment_status_dim_id DETERMINES (paymentstatus.payment_status_code)
;

/*Fact table*/
Create Table FactTable(
    fact_table_id int,
    flight_id int,
    booking_status_id int,
    passenger_id int,
    payment_method_id int,
    payment_status_id int,
    ticket_type_id int,
    travel_class_id int,
    booking_date date,
    payment_date date,
    booking_count int,
    total_payment int,
    avg_payment int,
    
    
    CONSTRAINT fact_table_id_fact_pk PRIMARY KEY (fact_table_id),
    CONSTRAINT flight_fact_fk_id FOREIGN KEY (flight_id) REFERENCES Flight(flight_id),
    CONSTRAINT booking_status_fk_id FOREIGN KEY (booking_status_id) REFERENCES BookingStatus(booking_status_id),
    CONSTRAINT passenger_fact_fk_id FOREIGN KEY (passenger_id) REFERENCES Passenger(passenger_id),
    CONSTRAINT payment_method_fact_fk_id FOREIGN KEY (payment_method_id) REFERENCES PaymentMethod(payment_method_id),
    CONSTRAINT payment_status_fact_fk_id FOREIGN KEY (payment_status_id) REFERENCES PaymentStatus(payment_status_id),
    CONSTRAINT tickettype_fact_fk_id FOREIGN KEY (ticket_type_id) REFERENCES tickettype(ticket_type_id),
    CONSTRAINT travel_class_fact_fk_id FOREIGN KEY (travel_class_id) REFERENCES TravelClass(travel_class_id),
    CONSTRAINT booking_date_fact_fk_id FOREIGN KEY (booking_date) REFERENCES TimeTable(date_id),
    CONSTRAINT payment_date_fact_fk_id FOREIGN KEY (payment_date) REFERENCES TimeTable(date_id)
)
PARTITION BY RANGE (booking_date)
(
PARTITION date_17 VALUES LESS THAN(TO_DATE('01/05/2017','DD/MM/YYYY')),
PARTITION date_18 VALUES LESS THAN(TO_DATE('01/04/2018','DD/MM/YYYY')),
PARTITION date_19 VALUES LESS THAN(TO_DATE('01/03/2019','DD/MM/YYYY')),
PARTITION date_20 VALUES LESS THAN(TO_DATE('01/02/2020','DD/MM/YYYY')),
PARTITION date_other  VALUES LESS THAN (MAXVALUE) 
);

/*Insert into time dimention*/
insert into timetable VALUES(TO_DATE('2020/02/03 21:02:44', 'yyyy/mm/dd hh24:mi:ss'),1,1,'jun','spr', 2020);
insert into timetable VALUES( TO_DATE('2019/01/16 12:00:00', 'yyyy/mm/dd hh:mi:ss'),1,1,'jun','spr',2019);
insert into timetable VALUES(TO_DATE('2020-03-03', 'yyyy-mm-dd'),1,1,'jun','spr',2020);
insert into timetable VALUES(TO_DATE('2019-05-05', 'yyyy-mm-dd'),1,1,'jun','spr',2019);

delete from timetable
insert into timetable VALUES(TO_DATE('2019/02/03 21:02:44', 'yyyy/mm/dd hh24:mi:ss'),1,1,'feb','spr',2019);
insert into timetable VALUES( TO_DATE('2019/01/16 12:00:00', 'yyyy/mm/dd hh:mi:ss'),1,1,'jun','spr',2019);
insert into timetable VALUES( TO_DATE('2020/02/02 12:00:00', 'yyyy/mm/dd hh:mi:ss'),1,1,'feb','spr',2020);
insert into timetable VALUES( TO_DATE('2020/02/5 12:00:00', 'yyyy/mm/dd hh:mi:ss'),1,1,'feb','spr',2020);


/*Insert into fact table*/
CREATE SEQUENCE FactSequence
MINVALUE 1
NOMAXVALUE
INCREMENT BY 1
START WITH 1
NOCACHE
ORDER
NOCYCLE
;

CREATE OR REPLACE TRIGGER FactTableForSequence
BEFORE INSERT
ON FactTable
FOR EACH ROW
BEGIN
 SELECT FactSequence.NEXTVAL
 INTO :NEW.fact_table_id
 FROM DUAL;
EXCEPTION
 WHEN OTHERS THEN
RAISE_APPLICATION_ERROR(-20111, 'Can not generate seq value');
END;
/

INSERT INTO facttable
(flight_id ,
    booking_status_id ,
    passenger_id ,
    payment_method_id ,
    payment_status_id ,
    ticket_type_id ,
    travel_class_id ,
    booking_date ,
    payment_date ,
    booking_count ,
    total_payment ,
    avg_payment 
    )
SELECT
   flight.flight_id  ,
  bookingstatus.booking_status_id ,
  passenger.passenger_id ,
 paymentmethod.payment_method_id ,
 paymentstatus.payment_status_id ,
  tickettype.ticket_type_id ,
    travelclass.travel_class_id ,
  CAST(booking.booking_date AS DATE) ,
     CAST(payment.payment_date AS DATE)  ,
    count(*) ,
    sum(payment_amount) ,
    avg( payment_amount)
FROM flight, bookingstatus, passenger, paymentmethod, paymentstatus,tickettype,travelclass,booking,payment
WHERE
booking.flight_id = flight.flight_id /* join booking and flight */
AND
booking.booking_status_id = bookingstatus.booking_status_id /* join booking and bookingstatus */
AND
booking.passenger_id = passenger.passenger_id /* join booking and passenger */
AND
booking.ticket_type_id = tickettype.ticket_type_id /* join booking and tickettype */
AND
booking.travle_class_id = travelclass.travel_class_id /* join booking and travelclass */
AND
payment.booking_id = booking.booking_id /* join payment and booking */
AND
payment.payment_method_id = paymentmethod.payment_method_id /* join payment and paymentmethod */
AND
payment.payment_status_id = paymentstatus.payment_status_id /* join payment and paymentstatus */
group by  flight.flight_id, bookingstatus.booking_status_id, passenger.passenger_id, paymentmethod.payment_method_id, 
paymentstatus.payment_status_id, tickettype.ticket_type_id, travelclass.travel_class_id, CAST(booking.booking_date AS DATE), CAST(payment.payment_date AS DATE) ;
/
COMMIT;

DELETE FROM FactTable;
select * from FactTable;


/*3.3*/
GRANT CREATE MATERIALIZED VIEW TO homeworku;
GRANT CREATE DATABASE LINK TO homeworku;

DROP MATERIALIZED VIEW MaterializedView;

/*Create materialized log*/
CREATE MATERIALIZED VIEW MaterializedView
TABLESPACE homeworkts
BUILD IMMEDIATE 
REFRESH COMPLETE
ON COMMIT
AS
SELECT CONCAT(p.first_name, p.last_name ) as full_name  ,
count(*) as booking_count,
sum(ft.total_payment) as total_payment
FROM FactTable ft, Passenger p
WHERE ft.passenger_id = p.passenger_id
GROUP BY CONCAT(p.first_name, p.last_name )
;

/*Create materialized log*/
CREATE MATERIALIZED VIEW LOG ON FactTable
WITH SEQUENCE, PRIMARY KEY, ROWID
INCLUDING NEW VALUES;


select * from MaterializedView;

select * from mlog$_FactTable;



/*3.5*/
SELECT  p.first_name, p.last_name, p.email,fact.booking_date, pm.payment_method_code,
sum(fact.total_payment), sum(sum(fact.total_payment)) over
(PARTITION BY first_name, payment_method_code ORDER BY sum(fact.total_payment)
RANGE BETWEEN UNBOUNDED PRECEDING AND CURRENT  ROW) as sum_all_payment

FROM FactTable fact, Passenger p, PaymentMethod pm
WHERE p.passenger_id = fact.passenger_id AND fact.payment_method_id = pm.payment_method_id
GROUP BY  p.first_name, p.last_name, p.email,fact.booking_date, pm.payment_method_code
;


/*3.6*/
SELECT tm.years,tm.months ,SUM(fact.total_payment)
FROM TimeTable tm ,FactTable fact
WHERE fact.payment_date = tm.date_id
GROUP BY ROLLUP(tm.years,tm.months )
ORDER BY  tm.months  NULLS FIRST , tm.years NULLS FIRST;


select * from facttable;