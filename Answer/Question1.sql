/*--------------------1.1--------------------*/
CREATE PFILE='E:\app\User\homework\PFILE' FROM SPFILE;

SHUTDOWN IMMEDIATE;

STARTUP NOMOUNT PFILE=E:\app\User\homework\PFILE;

ALTER DATABASE MOUNT;

ALTER DATABASE OPEN;

/*--------------------2.1--------------------*/
CREATE TEMPORARY TABLESPACE TempTableSpace TEMPFILE 'E:\app\User\homework\Tablespace\Temp Table Space' SIZE 100M;

/*--------------------3.1--------------------*/
CREATE TABLESPACE homeworkts DATAFILE 
'E:\app\User\homework\Tablespace\homeworkts1' SIZE 300M,
'E:\app\User\homework\Tablespace\homeworkts2' SIZE 300M;

/*change tablespace size*/
ALTER DATABASE DATAFILE 'E:\app\User\homework\Tablespace\HOMEWORKTS1' RESIZE 500M;
ALTER DATABASE DATAFILE 'E:\app\User\homework\Tablespace\HOMEWORKTS2' RESIZE 500M;


/*--------------------4.1--------------------*/
CREATE PROFILE homeworkpf
LIMIT 
SESSIONS_PER_USER 1
CONNECT_TIME 30
PASSWORD_GRACE_TIME 30
;

/*--------------------5.1--------------------*/
/*Grant connection and resource to homeworku*/
Grant connect to homeworku;
Grant resource to homeworku;

/*Create homeworku user*/
CREATE USER homeworku
IDENTIFIED BY homeworkpass
Default TableSpace homeworkts
TEMPORARY TABLESPACE TEMPTABLESPACE
PROFILE homeworkpf
Account unlock  
;
/*Create homewrokrole*/
CREATE ROLE homeworkrole NOT IDENTIFIED;

/*Grant ALL to hr table's*/
GRANT ALL ON HR.COUNTRIES TO homeworkrole;
GRANT ALL ON HR.DEPARTMENTS TO homeworkrole;
GRANT ALL ON HR.EMPLOYEES TO homeworkrole;
GRANT ALL ON HR.JOB_HISTORY TO homeworkrole;
GRANT ALL ON HR.JOBS TO homeworkrole;
GRANT ALL ON HR.LOCATIONS TO homeworkrole;
GRANT ALL ON HR.REGIONS TO homeworkrole;

/*Grant gomeworkrole to homeworku*/
GRANT homeworkrole TO homeworku;














