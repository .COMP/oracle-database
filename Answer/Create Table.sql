/*Passenger table, sequense, trigger*/
CREATE TABLE Passenger(
passenger_id NUMBER NOT NULL,
first_name VARCHAR(50),
last_name VARCHAR(50),
phone_no VARCHAR(50),
email VARCHAR(50),
address VARCHAR(50),
zip_code VARCHAR(50),
state VARCHAR(50),
city VARCHAR(50),
country VARCHAR(50),
CONSTRAINT passenger_id_pk PRIMARY KEY (passenger_id)
);

CREATE SEQUENCE PassengerSequence
MINVALUE 1
NOMAXVALUE
INCREMENT BY 1
START WITH 1
NOCACHE
ORDER
NOCYCLE
;
  
CREATE OR REPLACE TRIGGER PassengerTriggerForSequence
BEFORE INSERT
ON Passenger
FOR EACH ROW
BEGIN
 SELECT PassengerSequence.NEXTVAL
 INTO :NEW.passenger_id
 FROM DUAL;
EXCEPTION
 WHEN OTHERS THEN
RAISE_APPLICATION_ERROR(-20111, 'Can not generate seq value');
END;
/

/*Travel class table, sequence, trigger*/
CREATE TABLE TravelClass(
travel_class_id NUMBER NOT NULL,
travel_class_core VARCHAR(50),
CONSTRAINT travel_class_id_pk PRIMARY KEY (travel_class_id),
CONSTRAINT TravelClassConstraint 
CHECK (travel_class_core in('tourism',  'economical', 'first', 'business men')) 

);

CREATE SEQUENCE TravelClassSequence
MINVALUE 1
NOMAXVALUE
INCREMENT BY 1
START WITH 1
NOCACHE
ORDER
NOCYCLE
;
  
CREATE OR REPLACE TRIGGER TravelClassTriggerForSequence
BEFORE INSERT
ON TravelClass
FOR EACH ROW
BEGIN
 SELECT TravelClassSequence.NEXTVAL
 INTO :NEW.travel_class_id
 FROM DUAL;
EXCEPTION
 WHEN OTHERS THEN
RAISE_APPLICATION_ERROR(-20111, 'Can not generate seq value');
END;
/

/*Air plane table, sequence, trigger*/
CREATE TABLE AirPlane(
air_plane_id NUMBER NOT NULL,
air_craft_type VARCHAR(50),
CONSTRAINT air_plane_id_pk PRIMARY KEY (air_plane_id)
);

CREATE SEQUENCE AirPlaneSequence
MINVALUE 1
NOMAXVALUE
INCREMENT BY 1
START WITH 1
NOCACHE
ORDER
NOCYCLE
;
  
CREATE OR REPLACE TRIGGER AirPlaneTriggerForSequence
BEFORE INSERT
ON AirPlane
FOR EACH ROW
BEGIN
 SELECT AirPlaneSequence.NEXTVAL
 INTO :NEW.air_plane_id
 FROM DUAL;
EXCEPTION
 WHEN OTHERS THEN
RAISE_APPLICATION_ERROR(-20111, 'Can not generate seq value');
END;
/

/*Booking status table, sequence, trigger*/
CREATE TABLE BookingStatus(
booking_status_id NUMBER NOT NULL,
booking_status_code VARCHAR(50),
CONSTRAINT booking_status_id_pk PRIMARY KEY (booking_status_id),
CONSTRAINT BookingStatusConstraint 
CHECK (booking_status_code in ('refundable',  'not refundable')) 
);

CREATE SEQUENCE BookingStatusSequence
MINVALUE 1
NOMAXVALUE
INCREMENT BY 1
START WITH 1
NOCACHE
ORDER
NOCYCLE
;
  
CREATE OR REPLACE TRIGGER BookingStTriggerForSequence
BEFORE INSERT
ON BookingStatus
FOR EACH ROW
BEGIN
 SELECT BookingStatusSequence.NEXTVAL
 INTO :NEW.booking_status_id
 FROM DUAL;
EXCEPTION
 WHEN OTHERS THEN
RAISE_APPLICATION_ERROR(-20111, 'Can not generate seq value');
END;
/


/*Ticket type table, sequence, trigger*/
CREATE TABLE TicketType(
ticket_type_id NUMBER NOT NULL,
ticket_type_code VARCHAR(50),
CONSTRAINT ticket_type_id_pk PRIMARY KEY (ticket_type_id),
CONSTRAINT TicketTypeConstraint 
CHECK (ticket_type_code in ('active',  'not active')) 
);

/* For test*/
ALTER TABLE TicketType
DROP CONSTRAINT TicketTypeConstraint;


ALTER TABLE TicketType
ADD CONSTRAINT TicketTypeConstraint CHECK (ticket_type_code in ('active',  'not active'));


CREATE SEQUENCE TicketTypeSequence
MINVALUE 1
NOMAXVALUE
INCREMENT BY 1
START WITH 1
NOCACHE
ORDER
NOCYCLE
;
  
CREATE OR REPLACE TRIGGER TicketTypeTriggerForSequence
BEFORE INSERT
ON TicketType
FOR EACH ROW
BEGIN
 SELECT TicketTypeSequence.NEXTVAL
 INTO :NEW.ticket_type_id
 FROM DUAL;
EXCEPTION
 WHEN OTHERS THEN
RAISE_APPLICATION_ERROR(-20111, 'Can not generate seq value');
END;
/

/*Payment method*/
CREATE TABLE PaymentMethod(
payment_method_id NUMBER NOT NULL,
payment_method_code VARCHAR(50),
CONSTRAINT payment_method_id_pk PRIMARY KEY (payment_method_id),
CONSTRAINT PaymentMethodConstraint CHECK (payment_method_code in ('monetary',  'check', 'credit card')) 
);

CREATE SEQUENCE PaymentMethodSequence
MINVALUE 1
NOMAXVALUE
INCREMENT BY 1
START WITH 1
NOCACHE
ORDER
NOCYCLE
;
  
CREATE OR REPLACE TRIGGER PaymentMeTriggerForSequence
BEFORE INSERT
ON PaymentMethod
FOR EACH ROW
BEGIN
 SELECT PaymentMethodSequence.NEXTVAL
 INTO :NEW.payment_method_id
 FROM DUAL;
EXCEPTION
 WHEN OTHERS THEN
RAISE_APPLICATION_ERROR(-20111, 'Can not generate seq value');
END;
/

/*Payment status*/
CREATE TABLE PaymentStatus(
payment_status_id NUMBER NOT NULL,
payment_status_code VARCHAR(50),
CONSTRAINT payment_status_id_pk PRIMARY KEY (payment_status_id),
CONSTRAINT PaymentStatusConstraint CHECK (payment_status_code in ('paid off',  'Unpaid')) 
);

CREATE SEQUENCE PaymentStatusSequence
MINVALUE 1
NOMAXVALUE
INCREMENT BY 1
START WITH 1
NOCACHE
ORDER
NOCYCLE
;
  
CREATE OR REPLACE TRIGGER PaymentStTriggerForSequence
BEFORE INSERT
ON PaymentStatus
FOR EACH ROW
BEGIN
 SELECT PaymentStatusSequence.NEXTVAL
 INTO :NEW.payment_status_id
 FROM DUAL;
EXCEPTION
 WHEN OTHERS THEN
RAISE_APPLICATION_ERROR(-20111, 'Can not generate seq value');
END;
/

/*Flight*/
CREATE TABLE Flight(
flight_id NUMBER NOT NULL,
flight_number NUMBER,
destination VARCHAR(50),
departure_time TIMESTAMP,
arival_time TIMESTAMP,
air_plane_id NUMBER NOT NULL,
CONSTRAINT flight_id_pk PRIMARY KEY (flight_id),
CONSTRAINT air_plane_fk_to_air_plane_pk FOREIGN KEY (air_plane_id) REFERENCES AirPlane(air_plane_id)
);


CREATE SEQUENCE FlightSequence
MINVALUE 1
NOMAXVALUE
INCREMENT BY 1
START WITH 1
NOCACHE
ORDER
NOCYCLE
;
  
CREATE OR REPLACE TRIGGER FlightTriggerForSequence
BEFORE INSERT
ON Flight
FOR EACH ROW
BEGIN
 SELECT FlightSequence.NEXTVAL
 INTO :NEW.flight_id
 FROM DUAL;
EXCEPTION
 WHEN OTHERS THEN
RAISE_APPLICATION_ERROR(-20111, 'Can not generate seq value');
END;
/

/*Class seat capacity*/
CREATE TABLE ClassSeatCapacity(
travel_class_id NUMBER NOT NULL,
flight_id NUMBER NOT NULL,
seat_capacity VARCHAR(50),
CONSTRAINT travel_class_flight_id_pk PRIMARY KEY (travel_class_id, flight_id),
CONSTRAINT travel_c_fk_to_travel_c_pk FOREIGN KEY (travel_class_id) REFERENCES TravelClass(travel_class_id),
CONSTRAINT flight_c_fk_to_flight_c_pk FOREIGN KEY (flight_id) REFERENCES Flight(flight_id)
);
/

/*Booking*/
CREATE TABLE Booking(
booking_id NUMBER NOT NULL,
booking_date TIMESTAMP,
passenger_id NUMBER NOT NULL,
booking_status_id NUMBER NOT NULL,
ticket_type_id NUMBER NOT NULL,
flight_id NUMBER NOT NULL,
travle_class_id NUMBER NOT NULL,
CONSTRAINT booking_id_pk PRIMARY KEY (booking_id),
CONSTRAINT passenger_fk_to_passenger_pk FOREIGN KEY (passenger_id) REFERENCES Passenger(passenger_id),
CONSTRAINT booking_s_fk_to_booking_s_pk FOREIGN KEY (booking_status_id) REFERENCES BookingStatus(booking_status_id),
CONSTRAINT ticket_t_fk_to_ticket_t_pk FOREIGN KEY (ticket_type_id) REFERENCES TicketType(ticket_type_id),
CONSTRAINT flight_fk_to_flight_pk FOREIGN KEY (flight_id) REFERENCES Flight(flight_id),
CONSTRAINT travle_c_fk_to_travle_c_pk FOREIGN KEY (travle_class_id) REFERENCES TravelClass(travel_class_id)
);


CREATE SEQUENCE BookingSequence
MINVALUE 1
NOMAXVALUE
INCREMENT BY 1
START WITH 1
NOCACHE
ORDER
NOCYCLE
;
  
CREATE OR REPLACE TRIGGER BookingTriggerForSequence
BEFORE INSERT
ON Booking
FOR EACH ROW
BEGIN
 SELECT BookingSequence.NEXTVAL
 INTO :NEW.booking_id
 FROM DUAL;
EXCEPTION
 WHEN OTHERS THEN
RAISE_APPLICATION_ERROR(-20111, 'Can not generate seq value');
END;
/

/*Payment*/
CREATE TABLE Payment(
payment_id NUMBER NOT NULL,
payment_date TIMESTAMP,
payment_amount NUMBER,
payment_method_id NUMBER NOT NULL,
payment_status_id NUMBER NOT NULL,
booking_id NUMBER NOT NULL,
CONSTRAINT payment_id_pk PRIMARY KEY (payment_id),
CONSTRAINT payment_m_fk_to_payment_m_pk FOREIGN KEY (payment_method_id) REFERENCES PaymentMethod(payment_method_id),
CONSTRAINT payment_s_fk_to_payment_s_pk FOREIGN KEY (payment_status_id) REFERENCES PaymentStatus(payment_status_id),
CONSTRAINT booking_fk_to_booking_pk FOREIGN KEY (booking_id) REFERENCES Booking(booking_id)
);

CREATE SEQUENCE PaymentSequence
MINVALUE 1
NOMAXVALUE
INCREMENT BY 1
START WITH 1
NOCACHE
ORDER
NOCYCLE
;
  
CREATE OR REPLACE TRIGGER PaymentTriggerForSequence
BEFORE INSERT
ON Payment
FOR EACH ROW
BEGIN
 SELECT PaymentSequence.NEXTVAL
 INTO :NEW.payment_id
 FROM DUAL;
EXCEPTION
 WHEN OTHERS THEN
RAISE_APPLICATION_ERROR(-20111, 'Can not generate seq value');
END;
/



/* For example
insert into AirPlane  (air_craft_type) values('aa');
select * from AirPlane;
*/
